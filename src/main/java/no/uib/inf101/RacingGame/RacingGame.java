package no.uib.inf101.RacingGame;

import javax.swing.JFrame;

/**
 * The main class for the Racing Game program.
 * This class creates a JFrame window and adds a GamePanel to it,
 * which handles the game logic and rendering.
 * The window is displayed to the user and the game is started by
 * calling the start() method on the GamePanel.
 */


public class RacingGame {
    
    public static void main(String[] args) {
        JFrame frame = new JFrame("Racing Game");
        GamePanel gamePanel = new GamePanel();
        frame.add(gamePanel);
        frame.pack();
        frame.setResizable(false);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        gamePanel.start();
    }
}

