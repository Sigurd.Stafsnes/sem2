package no.uib.inf101.RacingGame;


import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KeyHandler extends KeyAdapter {

    private GamePanel game;
/**
 * Constructs a new KeyHandler object with the specified GamePanel object.
 * 
 * @param game the GamePanel object to be used
 */
    public KeyHandler(GamePanel game) {
        this.game = game;
    }
/**
 * Handles the key presses for the game.
 * 
 * @param e the KeyEvent object representing the key event
 */
    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_UP: // accelerate
                game.car.accelerate();
                break;
            case KeyEvent.VK_DOWN: // deaccelerate
                game.car.decelerate();
                break;
            case KeyEvent.VK_LEFT: // move left
                game.car.moveLeft();
                break;
            case KeyEvent.VK_RIGHT: // move right
                game.car.moveRight();
                break;
            case KeyEvent.VK_SPACE: // space when game over 
                if (game.gameOver) {
                    game.restart();
                }
                break;
        }
    }
}
