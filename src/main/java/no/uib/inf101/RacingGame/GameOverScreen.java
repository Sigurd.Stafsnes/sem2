package no.uib.inf101.RacingGame;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
/**
 * A class representing the game over screen in the Racing Game program.
 * Displays the player's final score and a message prompting the player
 * to play again by pressing the SPACE key.
 */

public class GameOverScreen {

    private int score;

    public GameOverScreen(int score) {
        this.score = score;
    }

    public void render(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, GamePanel.WIDTH, GamePanel.HEIGHT);
        g.setColor(Color.WHITE);
        g.setFont(new Font("Arial", Font.BOLD, 32));
        String gameOverText = "Game Over!";
        int textWidth = g.getFontMetrics().stringWidth(gameOverText);
        g.drawString(gameOverText, GamePanel.WIDTH / 2 - textWidth / 2, GamePanel.HEIGHT / 2 - 32);
        g.setFont(new Font("Arial", Font.PLAIN, 24));
        String scoreText = "Score: " + score;
        textWidth = g.getFontMetrics().stringWidth(scoreText);
        g.drawString(scoreText, GamePanel.WIDTH / 2 - textWidth / 2, GamePanel.HEIGHT / 2);
        g.drawString("Press SPACE to play again", GamePanel.WIDTH / 2 - 120, GamePanel.HEIGHT / 2 + 32);
    }
}

