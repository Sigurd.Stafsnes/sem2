package no.uib.inf101.RacingGame;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 * A class representing a car in the Racing Game program.
 * The car can be moved left or right, accelerated or decelerated,
 * and is drawn on a given Graphics object.
 * It also provides a method for getting the car's bounding Rectangle.
 */
public class Car {
    public static final int WIDTH = 50;
    public static final int HEIGHT = 100;

    public int x;
    public int y;
    public int speed;
    private Rectangle bounds;

        /**
     * Constructs a new Car object at the given coordinates.
     * The car's initial speed is set to 2 pixels per update cycle.
     * 
     * @param x the x-coordinate of the car's top-left corner
     * @param y the y-coordinate of the car's top-left corner
     */
    public Car(int x, int y) {
        this.x = x;
        this.y = y;
        this.speed = 2;
        this.bounds = new Rectangle(x, y, WIDTH, HEIGHT);
    }
    /**
     * Updates the car's position based on its current speed.
     * If the car goes off the top of the screen, it wraps around to the bottom.
     */
    public void update() {
        y -= speed;
        if (y < 0) {
            y = GamePanel.HEIGHT;
        }
        bounds.setLocation(x, y);
    }
        /**
     * Draws the car on the given Graphics object.
     * 
     * @param g the Graphics object to draw on
     */

    public void render(Graphics g) {
        g.setColor(Color.RED);
        g.fillRect(x, y, WIDTH, HEIGHT);
        g.setColor(Color.BLACK);
        g.fillRect(x + 10, y + 10, 10, 10);
        g.fillRect(x + 30, y + 10, 10, 10);
        g.fillRect(x + 10, y + 70, 10, 10);
        g.fillRect(x + 30, y + 70, 10, 10);
        g.setColor(Color.WHITE);
        g.fillRect(x + 20, y, 10, HEIGHT);
    }
        /**
     * Increases the car's speed by 1, up to a maximum of 10 pixels per update cycle.
     */

    public void accelerate() {
        speed = Math.min(speed + 1, 10);
    }
    /**  
     * Decreases the car's speed by 1*/
    
    public void decelerate() {
        if (speed > 2) { // Minimum 2 speed 
            speed = Math.max(speed - 1, 0);
        }
    }
    
    /**
     * Moves the car 10 pixels to the left, or to the edge of the screen if it would go offscreen.
     */
    public void moveLeft() {
        x -= 10;
        if (x < 0) {
            x = 0;
        }
        bounds.setLocation(x, y);
    }
    /**
     * Moves the car 10 pixels to the right, or to the edge of the screen if it would go offscreen.
     */
    public void moveRight() {
        x += 10;
        if (x + WIDTH > GamePanel.WIDTH) {
            x = GamePanel.WIDTH - WIDTH;
        }
        bounds.setLocation(x, y);
    }
    /**
     * Returns a Rectangle representing the car's bounding box.
     * 
     * @return the car's bounding box as a Rectangle object
     */
    public Rectangle getBounds() {
        return bounds;
    }
}
