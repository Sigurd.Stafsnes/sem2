package no.uib.inf101.RacingGame;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 * A utility class for handling graphics in the Racing Game program.
 * This class provides static methods for drawing the road, car, obstacles,
 * score, power-up, and power-up status on a given Graphics object.
 * It also defines constants for the width and height of obstacles and power-ups.
 */


public class GraphicsHandler {
    
    public final static int OBSTACLE_WIDTH = 50;
    public final static int OBSTACLE_HEIGHT = 50;
    public final static int POWERUP_WIDTH = 40;
    public final static int POWERUP_HEIGHT = 40;
    /**
     * Draws the road on the given Graphics object.
     * 
     * @param g the Graphics object to draw on
     * @param roadY the y-coordinate of the road's top edge
     * @param width the width of the drawing area in pixels
     * @param height the height of the drawing area in pixels
     */
    public static void drawRoad(Graphics g, int roadY, int width, int height) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, width, height);
        g.setColor(Color.WHITE);
        for (int i = 0; i < height; i += 50) {
            g.fillRect(width / 2 - 5, roadY + i, 10, 25);
        }
    }
        /**
     * Draws a car on the given Graphics object.
     * 
     * @param g the Graphics object to draw on
     * @param x the x-coordinate of the car's top-left corner
     * @param y the y-coordinate of the car's top-left corner
     * @param width the width of the car in pixels
     * @param height the height of the car in pixels
     */

    public static void drawCar(Graphics g, int x, int y, int width, int height) {
        g.setColor(Color.YELLOW);
        g.fillRect(x, y + height / 4, width, height / 2);
        g.setColor(Color.BLACK);
        g.drawRect(x, y + height / 4, width, height / 2);
        g.fillRect(x + width / 8, y + height / 8, width / 4, height / 4);
        g.fillRect(x + 5 * width / 8, y + height / 8, width / 4, height / 4);
        g.fillRect(x + width / 8, y + 5 * height / 8, width / 4, height / 4);
        g.fillRect(x + 5 * width / 8, y + 5 * height / 8, width / 4, height / 4);
    }
        /**
     * Draws a set of obstacles on the given Graphics object.
     * 
     * @param g the Graphics object to draw on
     * @param obstacles an array of Rectangles representing the obstacles to draw
     */

    public static void drawObstacles(Graphics g, Rectangle[] obstacles) {
        for (Rectangle obstacle : obstacles) {
            drawCar(g, obstacle.x, obstacle.y, obstacle.width, obstacle.height);
        }
    }
        /**
     * Draws the current score on the given Graphics object.
     * 
     * @param g the Graphics object to draw on
     * @param score the player's current score
     * @param x the x-coordinate of the score's top-left corner
     * @param y the y-coordinate of the score's top-left corner
     */

    public static void drawScore(Graphics g, int score, int x, int y) {
        g.setColor(Color.WHITE);
        g.drawString("Score: " + score, x, y);
    }
        /**
     * Draws a power-up on the given Graphics object.
     * 
     * @param g the Graphics object to draw on
     * @param powerUp a Rectangle representing the power-up to draw, or null if no power-up is active
     */

    public static void drawPowerUp(Graphics g, Rectangle powerUp) {
        if (powerUp != null) {
            g.setColor(Color.WHITE);
            g.fillOval(powerUp.x, powerUp.y, powerUp.width, powerUp.height);
            g.setColor(Color.BLACK);
            Font font = new Font("Arial", Font.BOLD, 20);
            g.setFont(font);
            g.drawString("x2", powerUp.x + 7, powerUp.y + 28);
        }
    }
            /**
     * Draws a message indicating whether the power-up is active on the given Graphics object.
     * 
     * @param g the Graphics object to draw on
     * @param powerUpActive true if the power-up is currently active, false otherwise
     * @param x the x-coordinate of the message's top-left corner
     * @param y the y-coordinate of the message's top-left corner
     */

    public static void drawPowerUpStatus(Graphics g, boolean powerUpActive, int x, int y) {
        g.setColor(Color.WHITE);
        Font font = new Font("Arial", Font.BOLD, 20);
        g.setFont(font);
        if (powerUpActive) {
            g.drawString("Power-up active!", x, y);
        }
    }
}
