package no.uib.inf101.RacingGame;


import java.io.InputStream;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

/**
 * Plays the sound with the given filename from the classpath.
 *
 * @param filename the name of the sound file to be played
 */

public class SoundPlayer {
    public static void playSound(String filename) {
        try {
            InputStream in = SoundPlayer.class.getResourceAsStream(filename);
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(in);
            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();
        } catch (Exception ex) {
            System.out.println("Error playing sound");
            ex.printStackTrace();
        }
    }
}
