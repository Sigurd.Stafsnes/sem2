package no.uib.inf101.RacingGame;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.JPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
/**
 * The GamePanel class represents the main panel of the game, where the game logic
 * and rendering are implemented. It extends the JPanel class and implements the Runnable
 * interface to allow the game loop to run on a separate thread.
 */

public class GamePanel extends JPanel implements Runnable {

    private static final long serialVersionUID = 1L;

    public final static int WIDTH = 800;
    public final static int HEIGHT = 600;
    public int speed = 5;
    public final static int POWERUP_DURATION = 15000; // 15 seconds in milliseconds
    
    private boolean running;
    private Thread thread;
    private BufferedImage image;
    private Graphics g;
    public Car car;
    public Rectangle[] obstacles;
    private int roadY;
    private Random random;
    public Score score;
    public boolean gameOver;
    private GameOverScreen gameOverScreen;
    public Rectangle powerUp;
    public static boolean powerUpActive;
    public Timer powerUpTimer;
        /**
     * Constructs a new GamePanel object.
     */

    public GamePanel() {
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        setBackground(Color.BLACK);
        setFocusable(true);
        requestFocus();
        addKeyListener(new KeyHandler(this));
        image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        g = image.getGraphics();
        car = new Car(WIDTH / 2 - Car.WIDTH / 2, HEIGHT - Car.HEIGHT - 10);
        obstacles = new Rectangle[0];
        roadY = 0;
        random = new Random();
        score = new Score();
        gameOver = false;
        gameOverScreen = new GameOverScreen(0);
        SoundPlayer.playSound("racetrack.wav");
        powerUp = null;
        powerUpActive = false;
        powerUpTimer = new Timer(POWERUP_DURATION, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                powerUpActive = false;
            }
        });
        powerUpTimer.setRepeats(false); // Ensures the timer only runs once
    }
    /**
     * Starts the game loop on a new thread.
     */

    public void start() {
        running = true;
        thread = new Thread(this);
        thread.start();
    }
    /**
     * Stops the game loop and waits for the thread to finish.
     */

    public void stop() {
        running = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    /**
     * Updates the game state.
     */

    public void update() {
        if (gameOver) {
            return;
        }
        car.update();
        List<Rectangle> obstacleList = new ArrayList<>();
        for (Rectangle obstacle : obstacles) {
            obstacle.y += speed;
            if (obstacle.y > HEIGHT) {
                score.addScore(10);
            } else if (car.getBounds().intersects(obstacle)) {
                gameOver = true;
                gameOverScreen = new GameOverScreen(score.getScore());
                return;
            } else {
                obstacleList.add(obstacle);
            }
        }
        obstacles = obstacleList.toArray(new Rectangle[0]);
        if (random.nextInt(100) < 1) {
            int x = random.nextInt(WIDTH - GraphicsHandler.OBSTACLE_WIDTH);
            int y = 0 - GraphicsHandler.OBSTACLE_HEIGHT;
            Rectangle obstacle = new Rectangle(x, y, GraphicsHandler.OBSTACLE_WIDTH, GraphicsHandler.OBSTACLE_HEIGHT);
            obstacleList.add(obstacle);
        }
        obstacles = obstacleList.toArray(new Rectangle[0]);
        if (random.nextInt(1000) < 1) { 
            if (powerUp == null) {
                int x = random.nextInt(WIDTH - GraphicsHandler.POWERUP_WIDTH);
                int y = 0 - GraphicsHandler.POWERUP_HEIGHT;
                powerUp = new Rectangle(x, y, GraphicsHandler.POWERUP_WIDTH, GraphicsHandler.POWERUP_HEIGHT);
            }
        }
        if (powerUp != null) {
            powerUp.y += speed;
            
            if (powerUp.y > HEIGHT) {
                powerUp = null;
            } else if (car.getBounds().intersects(powerUp)) {
                powerUpActive = true;
                powerUpTimer.restart();
                powerUp = null;
            }
        }
    }
    /**
     * Renders the game graphics.
     */
    
    public void render() {
        GraphicsHandler.drawRoad(g, roadY, WIDTH, HEIGHT);
        car.render(g);
        GraphicsHandler.drawObstacles(g, obstacles);
        GraphicsHandler.drawScore(g, score.getScore(), 20, 20);
    
        if (gameOver) {
            gameOverScreen.render(g);
        }
    
        GraphicsHandler.drawPowerUp(g, powerUp);
        GraphicsHandler.drawPowerUpStatus(g, powerUpActive, 20, 50);
    }
        
    /**
     * Paints the game graphics to the screen.
     *
     * @param g the graphics context to use for painting
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, null);
    }
    /**
     * Runs the game loop.
     */
    
    @Override
    public void run() {
        while (running) {
            update();
            render();
            repaint();
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        stop();
    }
    /**
     * Restarts the game with a new player car and reset score, obstacles, and power-up.
     */
    public void restart() {
        car = new Car(WIDTH / 2 - Car.WIDTH / 2, HEIGHT - Car.HEIGHT - 10);
        obstacles = new Rectangle[0];
        roadY = 0;
        score.reset();
        gameOver = false;
        powerUp = null;
        powerUpActive = false;
        powerUpTimer.stop();
    }    
}    