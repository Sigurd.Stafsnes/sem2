package no.uib.inf101.RacingGame;


    public class Score {
        private int score;

    /**
     * Constructs a new Score object with a score of 0.
     */
    public Score() {
        score = 0;
    }

    /**
     * Returns the current score.
     *
     * @return the current score
     */
    public int getScore() {
        return score;
    }

    /**
     * Adds the specified number of points to the score.
     * If a power-up is active, the points are doubled.
     *
     * @param points the number of points to add to the score
     */
    public void addScore(int points) {
        if (GamePanel.powerUpActive) {
            score += points * 2;
        } else {
            score += points;
        }
    }

    /**
     * Resets the score to 0.
     */
    public void reset() {
        score = 0;
        }
    }
