package no.uib.inf101;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import no.uib.inf101.RacingGame.Car;

public class AccelerateTest {
    
    @Test
    public void testAccelerate() {
        Car car = new Car(0, 0);
        
        // Test at hastigheten øker med 1 når vi kaller accelerate()
        int initialSpeed = car.speed;
        car.accelerate();
        int newSpeed = car.speed;
        assertEquals(initialSpeed + 1, newSpeed);
        
        // Test at hastigheten ikke går over 10
        for (int i = 0; i < 10; i++) {
            car.accelerate();
        }
        int maxSpeed = car.speed;
        assertEquals(10, maxSpeed);
    }
}
