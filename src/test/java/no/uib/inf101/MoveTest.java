package no.uib.inf101;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import no.uib.inf101.RacingGame.Car;
import no.uib.inf101.RacingGame.GamePanel;

public class MoveTest {
    @Test
    public void testMoveLeft() {
        Car car = new Car(50, 0);
        car.moveLeft();
        assertEquals(40, car.x);
        car.moveLeft();
        assertEquals(30, car.x);
        car.x = 0;
        car.moveLeft(); // x cannot be less than 0
        assertEquals(0, car.x);
    }
    
    @Test
    public void testMoveRight() {
        Car car = new Car(0, 0);
        car.moveRight();
        assertEquals(10, car.x);
        car.moveRight();
        assertEquals(20, car.x);
        car.x = GamePanel.WIDTH - Car.WIDTH;
        car.moveRight(); // x + WIDTH cannot be greater than GamePanel.WIDTH
        assertEquals(GamePanel.WIDTH - Car.WIDTH, car.x);
    }
    
}
