package no.uib.inf101;

import org.junit.jupiter.api.Test;

import no.uib.inf101.RacingGame.SoundPlayer;

public class SoundPlayerTest {
    
    @Test
    public void testPlaySound() {
        SoundPlayer.playSound("/sounds/engine.wav");
        try {
            Thread.sleep(2000); // Wait for the sound to finish playing
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}