# Mitt program

Se [oppgaveteksten](./OPPGAVETEKST.md) til semesteroppgave 2. Denne README -filen kan du endre som en del av dokumentasjonen til programmet, hvor du beskriver for en bruker hvordan programmet skal benyttes.

Her er link til videoen: https://www.youtube.com/watch?v=Ms2o5KINbuY

Unngå bilene/hindringene som kommer i full fart mot deg på veien i dette racingspillet! 
Spillet egner seg for alle aldersgrupper og er et utfordrende racingspill med høyt tempo der det krever at man konsentrerer seg. Det er mulig å få doble poeng i en viss periode dersom man plukker opp en "powerup". 

Keys: Man styrer bilen ved å bevege på piltastene der man og kan akselrere og senke farten ved hjelp av opp og ned piltastene. Det er bare å starte spillet på nytt med space tasten om man vil spille på nytt.